use std::convert::{AsRef, AsMut, From};

pub trait RegisterBase: From<u8> + Copy { }
pub trait Value<T> {
    fn value(self) -> T;
}
pub trait RegisterOperand<T>: Copy + Value<T>{ }

impl RegisterBase for u8 { }
impl RegisterBase for u16 { }
impl<T> RegisterOperand<T> for T
where
    T: RegisterBase + Value<T>
{ }
impl<T: RegisterBase> RegisterOperand<T> for Register<T> { }

impl Value<u8> for u8 {
    fn value(self) -> u8 {
        self
    }
}

impl Value<u16> for u16 {
    fn value(self) -> u16 {
        self
    }
}

impl<T> Value<T> for Register<T>
where
    T: RegisterBase
{
    fn value(self) -> T {
        *self.as_ref()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Register<T>
where
    T: RegisterBase
{
    value: T
}

impl<T> Register<T>
where
    T: RegisterBase
{
    pub fn new() -> Register<T> {
        Register::from(T::from(0))
    }

    fn ld<U>(&mut self, op: U)
    where
        U: RegisterOperand<T>
    {
        *self.as_mut() = op.value();
    }
}

impl<T> From<T> for Register<T>
where
    T: RegisterBase
{
    fn from(value: T) -> Register<T> {
        Register { value }
    }
}

impl<T> AsRef<T> for Register<T>
where
    T: RegisterBase
{
    fn as_ref(&self) -> &T {
        &self.value
    }
}

impl<T> AsMut<T> for Register<T>
where
    T: RegisterBase
{
    fn as_mut(&mut self) -> &mut T {
        &mut self.value
    }
}

#[cfg(test)]
mod tests {
    use crate::backend::cpu::register::*;

    #[test]
    fn ld_8bit_register() {
        let mut reg = Register::<u8>::new();
        reg.ld(38u8);
        assert_eq!(*reg.as_ref(), 38u8);
        reg.ld(22u8);
        assert_eq!(*reg.as_ref(), 22u8);
    }

    #[test]
    fn ld_16bit_register() {
        let mut reg = Register::<u16>::new();
        reg.ld(0xabcdu16);
        assert_eq!(*reg.as_ref(), 0xabcdu16);
        reg.ld(0x1122u16);
        assert_eq!(*reg.as_ref(), 0x1122u16);
    }
}
