#![warn(
    rust_2018_idioms,
    missing_debug_implementations,
    rustdoc::broken_intra_doc_links
)]

//! A Game Boy emulator running in the terminal

/// CPU emulation, input processing, etc. Essentially
/// everything that is not displayed graphically
pub mod backend;
